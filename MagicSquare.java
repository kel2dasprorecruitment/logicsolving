package projectcaslab;
import java.util.*;

public class MagicSquare {

    public static void main(String[] args) {
        Scanner arka = new Scanner(System.in);
        System.out.print("input ukuran matrix : ");
        int Ar = arka.nextInt();  
        int[][] matrix = new int[Ar][Ar];

        int cing = 0;        
        int kariak = 0;
        int k, j;        

        for(j=0; j<Ar; j++){
            for(k=0; k<Ar; k++){
                matrix[j][k] = arka.nextInt();
            }
            cing = cing + matrix[j][j];
            kariak = kariak + matrix[j][Ar-1-j];
        }

        int [] hitbaris = new int[Ar];
        int [] hitkolom = new int[Ar];

        for(j=0; j<Ar; j++){
            hitbaris[j] = 0;
            hitkolom[j] = 0;
            for(k=0; k <Ar; k++){
                hitbaris[j] = hitbaris[j] + matrix[j][k];
                hitkolom[j] = hitkolom[j] + matrix[k][j];
            }
        }

        ArrayList<Integer> list = new ArrayList<>();
        if(cing != kariak){
            list.add(0);
        }

        for(int i=0; i <Ar ; i++){
            if(hitbaris[i] != cing){
                list.add(i+1);
            }

            if(hitkolom[i] != cing){
                list.add(-(i+1));
            }
        }

        System.out.println(list.size());
        Collections.sort(list);

        for(int i=0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
    }
}