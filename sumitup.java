package dasrec;
import java.util.Arrays;
import java.util.Scanner;
/**
 *
 * @author Dwi Cahya Setyawan
 */
public class sumitup {
    public static void main(String[] args) {
        int arr, n, q;
        Scanner s = new Scanner(System.in);
        s.nextLine(); // Array N
        arr = Arrays.stream(s.nextLine().split("\\s+")).mapToInt(Integer::parseInt).sum();
        q = s.nextInt();
        System.out.println((Math.pow(2, q))*arr % ((Math.pow(10, 9))+7));
    }
    
}