/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
/**
*
* @author Dwi Cahya Setyawan
*/
package dasrec; 
 
import java.util.ArrayList; 
import java.util.List; 
import java.util.Scanner;
 
public class koderahasia { 
 
    public static void main(String[] args) { 
        Scanner input = new Scanner(System.in); 
        
        System.out.println("Masukkan kode rahasia : ");
        String kode = input.nextLine();
        //rubah .jadi0 -jadi1
        
        kode = kode.replace(".", "0"); 
        kode = kode.replace("-", "1"); 
        
        int jml = kode.length(); 
 
        //suffix di inverse = dibalik 
        String sfx = "---.--.-"; 
        sfx = sfx.replace("-","0"); 
        sfx = sfx.replace(".","1"); 
 
 
        String suffix = sfx; 
        int ASCI = Integer.parseInt(suffix,2); 
        String prefix = kode.substring(0,ASCI);  
  
        String isi = kode.substring(ASCI,((kode.length()-8))); 
 
        //a=8 bit
        String huruf = null; 
        int p = 0; 
        int a = 8; 
 
        System.out.println(" "); 
        System.out.println("Hasil : "); 
 
        for (int i = 0; i<=isi.length(); i++){ 
             
            if(a<576 && p<576){ 
                String bit = isi.substring(p, a); 
                int ASCI2 = Integer.parseInt(bit,2); 
                //System.out.println(bit + " = " +ASCI2 + " = " +(char)ASCI2); 
                System.out.print((char)ASCI2); 
                p = p +8; 
                a = a+8; 
            }  
        } 
        System.out.println(" "); 
    } 
}